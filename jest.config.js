module.exports = {
  roots: ['./src', './__mocks__'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  }
};
