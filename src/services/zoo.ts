import { Animal } from '../db';

export async function leaderboard(): Promise<any> {
  const leaders = await Animal.findAll({
    order: [['likes', 'DESC']]
  });
  return { leaders };
}
