import axios from 'axios';

export async function countWord(
  url: string,
  word: string
): Promise<number> {
  const resp = await axios({
    method: 'get',
    url,
    responseType: 'text'
  });

  const re = new RegExp(word, 'gi');
  return (resp.data.match(re) || []).length;
}

export async function doNotUsePromiseAll() {
  // return Promise.resolve(42);
  // return new Promise((resolve) => {
  //   ERROR HERE?
  //   resolve(42);
  // })
  if (2 > 3) {
    return 41;
  }
  if (2 + 2 === 5) {
    throw new Error('OUCH');
  }
  return 42;
}
