import axios from 'axios';

const baseUrl = 'http://api.angularbootcamp.com/';

const empPath = 'employees';

const todoPath = 'todos';

async function count(path: string): Promise<number> {
  const response = await axios.get<any[]>(
    baseUrl + path
  );
  return response.data.length;
}

export async function employeeCount(): Promise<
  number
> {
  return count(empPath);
}

export async function todoCount(): Promise<number> {
  return count(todoPath);
}

// Consider:
// https://github.com/RasCarlito/axios-cache-adapter
// https://github.com/kuitos/axios-extensions
