import axios from 'axios';
import { from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

const baseUrl = 'http://api.angularbootcamp.com/';

interface Todo {
  userId: number;
}

async function fetchUser(id: number) {
  const url = `${baseUrl}users/${id}`;
  console.log('Fetching ', id);
  const response = await axios.get(url);
  console.log('Done     ', id);
  return response.data;
}

export async function usersWithCompletedWork(): Promise<
  number[]
> {
  const todoApiUrl = baseUrl + 'todos?completed=true';
  const todos: Todo[] = (await axios.get(todoApiUrl))
    .data;
  const userIdSet = new Set<number>();
  for (const todo of todos) {
    userIdSet.add(todo.userId);
  }
  const userIds = Array.from(userIdSet);

  // return Promise.all(userIds.map(id => fetchUser(id)));

  return from(userIds)
    .pipe(mergeMap(id => fetchUser(id), 3))
    .toPromise();
}
