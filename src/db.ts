import {
  Sequelize,
  Model,
  DataTypes
} from 'sequelize';

const connString =
  'postgres://user1:12345bbb@35.226.141.212/zoo';

export const sequelize = new Sequelize(connString);

export class Animal extends Model {
  name!: string;
  likes!: number;
  limbs!: number;
}

Animal.init(
  {
    name: DataTypes.STRING,
    likes: DataTypes.NUMBER,
    limbs: DataTypes.NUMBER
  },
  {
    sequelize,
    modelName: 'animal'
  }
);
