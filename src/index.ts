import express from 'express';

import { handler as rootHandler } from './routes/root';
import { handler as employeeHandler } from './routes/employees';
import { handler as wordCountHandler } from './routes/wordCount';
import { handler as statsHandler } from './routes/stats';
import { handler as zooLeaderBoardHandler } from './routes/zoo';

const app = express();
const port = 3000;

// Routing table
app.get('/', rootHandler);
app.get('/employees', employeeHandler);
app.get('/word', wordCountHandler);
app.get('/stats', statsHandler);
app.get('/zoo', zooLeaderBoardHandler);

app.listen(port, () =>
  console.log(`Example app listening on port ${port}!`)
);
