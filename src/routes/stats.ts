import { Request, Response } from 'express';

import { usersWithCompletedWork } from '../services/todo';

export async function handler(
  req: Request,
  res: Response
) {
  try {
    // orchestrate here, in this example
    const users = await usersWithCompletedWork();
    res.send({ users });
  } catch (error) {
    console.error(error);
  }
}
