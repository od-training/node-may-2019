import { Request, Response } from 'express';

import { countWord } from '../services/pageAnalysis';

export async function handler(
  req: Request,
  res: Response
) {
  try {
    const url = 'https://www.yahoo.com/';
    const word = 'sports';
    const n = await countWord(url, word);

    // res.header('Cache-Control: public, max-age=60');

    res.send({ n });
  } catch (error) {
    console.error(error);
  }
}
