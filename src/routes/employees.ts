import { Request, Response } from 'express';

import * as hr from '../services/hr';

export async function handler(
  req: Request,
  res: Response
) {
  try {
    const employeePromise = hr.employeeCount();
    const todoPromise = hr.todoCount();
    const employeeCount = await employeePromise;
    let todoCount: number | string = 'UNKNOWN';
    try {
      todoCount = await todoPromise;
    } catch (error) {
      console.log(
        'proceeding in spite of TODO API failure'
      );
    }
    res.send({
      employeeCount,
      todoCount
    });
  } catch (error) {
    console.error(error);
  }
}
