import { Request, Response } from 'express';

import { leaderboard } from '../services/zoo';

export async function handler(
  req: Request,
  res: Response
) {
  try {
    const board = await leaderboard();
    res.send(board);
  } catch (error) {
    console.error(error);
  }
}
