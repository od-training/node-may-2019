import yargs from 'yargs';

import { countWord } from './services/pageAnalysis';

const argv: any = yargs
  .usage('Usage: $0 -u [str] -w [str]')
  .demandOption(['u', 'w']).argv;

async function run() {
  const n = await countWord(argv.u, argv.w);
  console.log(n);
}

run();
